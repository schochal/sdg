import React from 'react';
import ReactDOM from 'react-dom';

import {
  Box,
  Card,
  CardContent,
  Typography,
	Tabs,
	Tab,
} from '@mui/material';

import {Masonry} from '@mui/lab';

import { ThemeProvider } from '@mui/material/styles';

import theme from './theme';

import { 
  Container
} from '@mui/material';

import BlogForm from './blogForm';


function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box>
          {children}
        </Box>
      )}
    </div>
  );
}

class Blog extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
			user: {},
      posts: [],
    }

		this.getUser = this.getUser.bind(this);
    this.getData = this.getData.bind(this);
		this.alreadyHasAPost = this.alreadyHasAPost.bind(this);
  }

  componentDidMount() {
    this.getData();
    this.getUser();
  }

  async getData() {
    const response = await fetch('/main-api');
    const data = await response.json();
		let tabs = [];
		data.map(post => {
			tabs.push(0);	
		});
    this.setState({
      posts: data,
			tabs: tabs,
    });
  }

	async getUser() {
    const response = await fetch('/user-api');
    const data = await response.json();
    this.setState({
      user: data,
    });
  }

	getNames(users) {
		let names = '';
		users.map(user => {	
			names = names + ', ' + user.firstName + ' ' + user.lastName;
		});
		return names.substring(2, names.length);
	}

	alreadyHasAPost() {
		let result = false;
		this.state.posts.map(post => {
			if(post.team.id == this.state.user.team.id) {
				result = true;
			}
		});
		return result;
	}

  render () { 
    return (
      <Container maxWidth="xl">
				{ Object.entries(this.state.user).length > 0 &&
					<BlogForm hasAPost={this.alreadyHasAPost()} getData={this.getData}/>
				}

        <ThemeProvider theme={theme}>
          <Box style={{ marginTop: '20px', marginRight: '-16px' }}>
            <Masonry columns={{sx: 1, sm: 2, md: 3}} spacing={2}>
              {this.state.posts.map((post,index) => (
                <Card key={post.timestamp} elevation={6}>
                  <CardContent>
                    <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                      {post.timestamp}
                    </Typography>
                    <Typography variant="h5" gutterBottom>
                      {post.compound1} / {post.compound2}
                    </Typography>
                    <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                      by {this.getNames(post.team.users)}
                    </Typography>
										<Typography style={{ marginBottom: '10px' }}>
											Δ<sub>v</sub><i>H</i><sub>{post.compound1}</sub> = {post.DvH} ± {post.DvH_ci} kJ mol<sup>-1</sup><br/>
											<i>T</i><sub>b,{post.compound1}</sub> = {post.Tb} ± {post.Tb_ci} °C<br/>
											Δ<sub>v</sub><i>S</i><sub>{post.compound1}</sub> = {post.DvS} ± {post.DvS_ci} J mol<sup>-1</sup> K<sup>-1</sup><br/>
										</Typography>
										<Typography>
											Δ<sub>v</sub><i>H</i><sub>{post.compound2}</sub> = {post.DvH2} ± {post.DvH2_ci} kJ mol<sup>-1</sup><br/>
											<i>T</i><sub>b,{post.compound2}</sub> = {post.Tb2} ± {post.Tb2_ci} °C<br/>
											Δ<sub>v</sub><i>S</i><sub>{post.compound2}</sub> = {post.DvS2} ± {post.DvS2_ci} J mol<sup>-1</sup> K<sup>-1</sup><br/>
										</Typography>
										<Tabs 
											value={this.state.tabs[index]}
											onChange={(event, newValue) => { let tabs=this.state.tabs; tabs[index] = newValue; this.setState({tabs: tabs,}); }}
											centered
										>
											<Tab label="Exponential" />
											<Tab label="Linear" />
										</Tabs>
										<TabPanel value={this.state.tabs[index]} index={0}>
											<img
												style={{ width: '100%', borderRadius: '5px', marginTop: '10px' }}
												src={"/media/exp_" + post.image}
											/>
										</TabPanel>
										<TabPanel value={this.state.tabs[index]} index={1}>
											<img
												style={{ width: '100%', borderRadius: '5px', marginTop: '10px' }}
												src={"/media/lin_" + post.image}
											/>
										</TabPanel>
                  </CardContent>
                </Card>
              ))}
            </Masonry>
          </Box>
        </ThemeProvider>
      </Container>
    )
  }
}

ReactDOM.render(<Blog/>, document.getElementById('main'));
