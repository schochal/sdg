<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ClausiusClapeyronController extends AbstractController
{
    #[Route('/clausius-clapeyron', name: 'clausius-clapeyron')]
    public function index(): Response
    {
        return $this->render('clausius-clapeyron/index.html.twig');
    }
}
